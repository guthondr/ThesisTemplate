LaTeX template for theses at FIT CTU (retired)
==============================================

This project is obsoleted by https://gitlab.fit.cvut.cz/theses-templates/FITthesis-LaTeX. New features are unlikely to be added.


Download
--------

Access [the latest version here](https://gitlab.fit.cvut.cz/guthondr/ThesisTemplate/-/jobs/artifacts/master/browse?job=all-zip).


About
-----

Template for bachelors' and masters' (diploma) theses submitted at [Faculty of Information Technology](https://fit.cvut.cz) at [Czech Technical University in Prague](https://cvut.cz).


Authors
-------

* Ondřej Guth
* Jan Holub


License
-------

All rights reserved. Permission to use outside FIT CTU may be obtained from [vice-dean for study affairs](https://fit.cvut.cz/en/studies/information-service/office-of-study-affairs/people).

![pipeline status](https://gitlab.fit.cvut.cz/guthondr/ThesisTemplate/badges/master/pipeline.svg)
